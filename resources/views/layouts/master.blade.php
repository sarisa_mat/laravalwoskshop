<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   @section('css')
        <link rel="stylesheet" href="{{asset ('css/main.css')}}">
   @show
    <title>@yield('title')</title>
</head>
<body>
    <div class="conrainer">
        <h1 class="main-red"> green </h1>
    
    @include('layouts/navbar')

    @yield('content')

    @yield('js')
    
    
    </div>
</body>
</html>