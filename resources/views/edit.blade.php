@extends('layouts.master')
@section('title','add')
@section('content')
<form action ="{{ url('people', [$people->id] ) }}" method="post">
    @csrf
    @method("put")
    <div class="form-group">
        <label>Firstname </label>
        <input value="{{ $people->fname }}" type="text " class= "form-control" name="fname">
    </div>

    <div class="form-group">
        <label>Lastname </label>
        <input value="{{ $people->lname }}" type="text " class= "form-control" name="lname">
    </div>

    <div class="form-group">
        <label>age</label>
        <input value="{{ $people->age}}" type="text " class= "form-control" name="age">
    </div>

    <button type="submit" class=" btn btn-success" >Save</button>
    @if($errors->any())
    <div class ="alert alert-danger">
    <ul>
        @foreach($errors ->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
    @endif
</form>

@endsection