@extends('layouts.master')
@section('title','list')
@section ('css')

@parent
    <link rel="stylesheet" href="{{ asset ('css/main.css') }}">
@endsection

@section('content')
    @if (Session :: has('message'))
        <div class="alert alert-sucess">
            {{Session ::get('message')}}
        </div>
    @endif

    <a href = "{{url('people/created')}}">
        <button type=" button" class="btn btn-success">Create</button>
    </a>

    <h1 class="main-red"">List Page </h1>   
    <table>
        <thead>
            <th>ID</th>
            <th>Firstname</th>
            <th>lastname</th>g
            <th>AGE</th>
            <th>Created_at</th>
            <th>Upadted_at</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach ($people as $p)
            <tr>
                <td>{{ $p->id }}</td>
                <td>{{ $p->fname }}</td>
                <td>{{ $p->lname}}</td>
                <td>{{ $p->age}}</td>
                <td>{{ $p ->created_at}}</td>
                <td>{{ $p ->updated_at}}</td>

                <td>
                    <div class="form-inline">
                    <a = href="{{ url ('people/'.$p->id .'/edit')}}">
                    <button type="submit" class="btn btn-danger">edit</button>
                    </a>
            
            <form action="{{url('people/'.$p->id)}}" method="post">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger">delete</button>

            </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <!-- เรียกใช้ css  main-red  ไฟล์ main.css -->
@endsection