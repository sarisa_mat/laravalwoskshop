<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Symfony\Component\Routing\Router;

Route::get('/', function () {
    return view('layouts.master');
});

Route::get('/list', function () {
    return view('list');
});

Route::get('/add', function () {
    return view('add');
});


Route::resource('/people','PeopleController');


// Route::get('/user', function () {
//     return view('user');
// });

// Route::view ('/user','user');

// Route::get('/user/{id}', function ($id) {
//         return view('users.user') ->with('id',$id);
//     });

// Route::get('user/{id}','UserController@index');


// Route::prefix('user')->group(function () { 
//     Route::get('{id}', 'productController@index'); 
//     Route::get('{id}/test', function() {
//         return "test";
//     }) ->where ('id','[0-9]_');
// });






// Route::get('user/create','UserController@index');
// Route::get('user/edit','UserController@index');
// Route::get('user/delete','UserController@index');
